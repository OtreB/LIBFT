/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 17:42:26 by cumberto          #+#    #+#             */
/*   Updated: 2017/05/12 14:44:01 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*convert_oct(uintmax_t num)
{
	char		*str;
	int			cnt;
	uintmax_t	tmp;

	cnt = 1;
	tmp = num;
	while ((tmp = tmp / 8) != 0)
		cnt++;
	str = ft_strnew(cnt);
	while (cnt--)
	{
		str[cnt] = (num % 8) + 48;
		num = num / 8;
	}
	return (str);
}

int		ft_print_oct(t_print *elem, va_list *arg)
{
	char		*str;
	int			size;
	int			max;
	uintmax_t	num;

	num = manage_unsigned_mod(elem, *arg);
	if (num == 0 && elem->prec == 0 && elem->flag_h != 2)
		str = "";
	else
		str = convert_oct(num);
	if (elem->flag_h == 2 && num > 0)
		str = ft_strjoin("0", str);
	size = ft_strlen(str);
	max = elem->prec > size ? elem->prec : size;
	print_str_und(elem, str, size, max);
	if (str[0] != '\0')
		free(str);
	if (elem->width > max)
		max = elem->width;
	return (max);
}
