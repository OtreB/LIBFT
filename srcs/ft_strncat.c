/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 15:11:07 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/05 18:44:59 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dst, const char *src, size_t n)
{
	int		i;
	size_t	k;

	i = 0;
	k = 0;
	while (dst[i])
		i++;
	while (k < n && src[k])
	{
		dst[i] = src[k];
		i++;
		k++;
	}
	while (k <= n)
	{
		dst[i] = '\0';
		k++;
	}
	return (dst);
}
