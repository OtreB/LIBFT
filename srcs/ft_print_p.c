/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_p.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 14:56:01 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 12:33:29 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*convert_hex_pnt(unsigned long num)
{
	char			*str;
	int				cnt;
	unsigned long	tmp;

	cnt = 1;
	tmp = num;
	while ((tmp = tmp / 16) != 0)
		cnt++;
	str = ft_strnew(cnt);
	while (cnt--)
	{
		if ((tmp = (num % 16) + 48) > 57)
			tmp += 39;
		str[cnt] = tmp;
		num = num / 16;
	}
	return (str);
}

void	print_str_pnt(t_print *elem, char *str, int size, int max)
{
	if (elem->flag_m == 1 || elem->flag_z == 1)
		write(g_fd, "0x", 2);
	if (elem->width > max)
	{
		if (elem->flag_m == 1)
		{
			if (elem->prec > size)
				print_char('0', elem->prec - size);
			write(g_fd, str, size);
			print_char(' ', elem->width - max - 2);
			return ;
		}
		else
			print_char(elem->chr, elem->width - max - 2);
	}
	if (elem->flag_z != 1 && elem->flag_m != 1)
		write(g_fd, "0x", 2);
	if (elem->prec > size)
		print_char('0', elem->prec - size);
	write(g_fd, str, size);
}

int		ft_print_pnt(t_print *elem, va_list arg)
{
	int		max;
	char	*str;
	int		size;

	size = 0;
	if (elem->prec == 0)
	{
		ft_putstr("0x");
		return (2);
	}
	str = convert_hex_pnt((unsigned long)va_arg(arg, void*));
	size = ft_strlen(str);
	max = elem->prec > size ? elem->prec : size;
	print_str_pnt(elem, str, size, max);
	free(str);
	if (elem->prec > size)
		size = elem->prec;
	if (elem->width > size + 2)
		size = elem->width;
	else
		size += 2;
	return (size);
}
